use clap::Parser;
use rustyline::error::ReadlineError;
use rustyline::{Editor, Result};
use std::num::IntErrorKind;
use std::ops::{Index, IndexMut};
use std::str::{FromStr, SplitWhitespace};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long, default_value = None)]
    expression: Option<String>,
}

const PROMPT: &str = ">> ";
const HISTORY_FILE: &str = ".history";

#[derive(Debug, Copy, Clone)]
enum OpCode {
    // Internals
    Push(isize),
    Call(usize),
    Exec,

    // Arithmetic
    Add,
    Sub,
    Mul,
    Div,
    Mod,

    // Stack manipulation
    Dup,
    Drop,
    Swap,
    Rot,

    // Debugging and display
    Print,
    ShowStack,

    // Parsing
    ParseName,

    // Dictionary
    NewDictEntry,
    LookupDictEntry,

    // Modes
    CompileMode,
    InterpretMode,
}

enum Mode {
    Interpreting,
    Compiling,
}

struct ParseNum(isize);
impl FromStr for ParseNum {
    type Err = IntErrorKind;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut si = s.chars();
        let c = si.next();
        let mut out = 0isize;
        let is_neg = match c {
            Some('-') => true,
            Some(chr) if chr.is_digit(10) => {
                out += chr.to_digit(10).unwrap() as isize;
                false
            }
            _ => {
                return Err(IntErrorKind::Empty);
            }
        };
        while let Some(c) = si.next() {
            match c.to_digit(10) {
                Some(d) => out = out * 10 + d as isize,
                None => return Err(IntErrorKind::InvalidDigit),
            }
        }
        Ok(ParseNum(if is_neg { -out } else { out }))
    }
}

struct Xt {
    latest: u32,
    here: u32,
}
impl Into<isize> for Xt {
    fn into(self) -> isize {
        let high = self.latest.to_be_bytes();
        let low = self.here.to_be_bytes();
        let bytes: [u8; 8] = unsafe { std::mem::transmute([low, high]) };
        isize::from_be_bytes(bytes)
    }
}
impl From<isize> for Xt {
    fn from(value: isize) -> Self {
        let bytes = value.to_be_bytes();
        let (low, high) = bytes.split_at(4);
        Self {
            here: u32::from_be_bytes(low.try_into().unwrap()),
            latest: u32::from_be_bytes(high.try_into().unwrap()),
        }
    }
}

/// A token is either a number literal or a word
/// A word is an executable piece of forth code bound to a name.
/// A number is a signed integer matching `(-)?[0-9]+`
#[derive(PartialEq, Eq, Debug)]
pub enum Token<'a> {
    Word(usize, bool),
    Num(isize),
    Undefined(&'a str),
}

struct Lexer<'a> {
    source: SplitWhitespace<'a>,
}
impl<'a> Lexer<'a> {
    fn new(tokens: &'a str) -> Self {
        Self {
            source: tokens.split_whitespace(),
        }
    }

    fn next(&mut self, dict: &Dictionary) -> Option<Token<'a>> {
        self.next_raw().map(|tok| {
            dict.lookup(tok)
                .map(|(idx, imm)| Token::Word(idx, imm))
                .or_else(|| tok.parse::<ParseNum>().map(|pn| Token::Num(pn.0)).ok())
                .unwrap_or_else(|| Token::Undefined(tok))
        })
    }

    fn next_raw(&mut self) -> Option<&'a str> {
        self.source.next()
    }
}

type Stack = Vec<isize>;

#[derive(Clone)]
struct WordFlags {
    immediate: bool,
}

#[derive(Clone)]
struct Word {
    name: String,
    flags: WordFlags,
    instrs: Vec<OpCode>,
}
impl Word {
    pub(crate) fn new<S: ToString>(name: S, instrs: Vec<OpCode>) -> Self {
        Self {
            name: name.to_string(),
            flags: WordFlags { immediate: false },
            instrs,
        }
    }
    pub(crate) fn new_immediate<S: ToString>(name: S, instrs: Vec<OpCode>) -> Self {
        Self {
            name: name.to_string(),
            flags: WordFlags { immediate: true },
            instrs,
        }
    }
}

type WordIndex = usize;
struct Dictionary {
    items: Vec<Word>,
}

impl Dictionary {
    pub fn lookup(&self, target: &str) -> Option<(WordIndex, bool)> {
        self.items
            .iter()
            .enumerate()
            .rev()
            .find_map(|(index, word)| (word.name == target).then(|| (index, word.flags.immediate)))
    }

    fn insert<S: ToString>(&mut self, name: S, instrs: Vec<OpCode>) -> usize {
        let idx = self.items.len();
        self.items.push(Word::new(name.to_string(), instrs));
        idx
    }
}
impl FromIterator<Word> for Dictionary {
    fn from_iter<T: IntoIterator<Item = Word>>(iter: T) -> Self {
        Self {
            items: iter.into_iter().collect(),
        }
    }
}

impl Index<WordIndex> for Dictionary {
    type Output = Word;
    fn index(&self, index: WordIndex) -> &Self::Output {
        &self.items[index]
    }
}
impl IndexMut<WordIndex> for Dictionary {
    fn index_mut(&mut self, index: WordIndex) -> &mut Self::Output {
        &mut self.items[index]
    }
}

struct Machine {
    mode: Mode,
    stack: Stack,
    dict: Dictionary,
    /// An ethereal space to hold a name for parsing words
    name_word: Option<String>,
}

impl Default for Machine {
    fn default() -> Machine {
        use OpCode::*;

        Machine {
            mode: Mode::Interpreting,
            stack: vec![],
            name_word: None,
            dict: Dictionary::from_iter([
                Word::new("true", vec![Push(1)]),
                Word::new("false", vec![Push(0)]),
                Word::new("+", vec![Add]),
                Word::new("-", vec![Sub]),
                Word::new("*", vec![Mul]),
                Word::new("/", vec![Div]),
                Word::new("%", vec![Mod]),
                Word::new("dup", vec![Dup]),
                Word::new("drop", vec![Drop]),
                Word::new("swap", vec![Swap]),
                Word::new("rot", vec![Rot]),
                Word::new(".", vec![Print]),
                Word::new(".s", vec![ShowStack]),
                Word::new(":", vec![ParseName, NewDictEntry, CompileMode]),
                Word::new("'", vec![ParseName, LookupDictEntry]),
                Word::new("execute", vec![Exec]),
                Word::new_immediate(";", vec![Drop, InterpretMode]),
            ]),
        }
    }
}

impl Machine {
    fn exec(&mut self, ops: &[OpCode], input: &mut Lexer<'_>) {
        use OpCode::*;
        let mut ip = 0;
        loop {
            if ip >= ops.len() {
                break;
            }
            match ops[ip] {
                Push(i) => self.stack.push(i),
                ParseName => {
                    let name = input.next_raw().expect("Empty input, expected a name");
                    self.name_word = Some(name.to_string());
                }
                NewDictEntry => {
                    let name = self.name_word.take().expect("Empty input, expected a name");
                    let word_idx = self.dict.insert(name, vec![]);
                    self.stack.push(word_idx.try_into().unwrap());
                }
                LookupDictEntry => {
                    let name = self.name_word.take().expect("Empty input, expected a name");
                    let xt = self
                        .dict
                        .lookup(&name)
                        .map(|(idx, _)| idx)
                        .expect("Undefined behavior: Look up non");
                    self.stack.push(xt.try_into().unwrap());
                }
                Exec => {
                    if self.stack.is_empty() {
                        // Error
                        unimplemented!()
                    }
                    let idx = self.stack.pop().unwrap() as usize;
                    let word = &self.dict[idx].clone();
                    self.exec(&word.instrs, input);
                }
                Call(idx) => {
                    let word = &self.dict[idx].clone();
                    self.exec(&word.instrs, input);
                }
                Add => {
                    if self.stack.len() < 2 {
                        // Error
                        unimplemented!();
                    }

                    let rhs = self.stack.pop().unwrap();
                    let lhs = self.stack.pop().unwrap();
                    self.stack.push(lhs + rhs);
                }
                Sub => {
                    if self.stack.len() < 2 {
                        // Error
                        unimplemented!();
                    }

                    let rhs = self.stack.pop().unwrap();
                    let lhs = self.stack.pop().unwrap();
                    self.stack.push(lhs - rhs);
                }
                Mul => {
                    if self.stack.len() < 2 {
                        // Error
                        unimplemented!();
                    }

                    let rhs = self.stack.pop().unwrap();
                    let lhs = self.stack.pop().unwrap();
                    self.stack.push(lhs * rhs);
                }
                Div => {
                    if self.stack.len() < 2 {
                        // Error
                        unimplemented!();
                    }

                    let rhs = self.stack.pop().unwrap();
                    let lhs = self.stack.pop().unwrap();
                    self.stack.push(lhs / rhs);
                }
                Mod => {
                    if self.stack.len() < 2 {
                        // Error
                        unimplemented!();
                    }

                    let rhs = self.stack.pop().unwrap();
                    let lhs = self.stack.pop().unwrap();
                    self.stack.push(lhs % rhs);
                }
                Dup => {
                    if self.stack.is_empty() {
                        // Error
                        unimplemented!();
                    }

                    self.stack.push(*self.stack.last().unwrap());
                }
                Drop => {
                    if self.stack.is_empty() {
                        // Error
                        unimplemented!();
                    }

                    self.stack.pop();
                }
                Swap => {
                    if self.stack.len() < 2 {
                        // Error
                        unimplemented!();
                    }

                    let snd = self.stack.pop().unwrap();
                    let fst = self.stack.pop().unwrap();
                    self.stack.push(snd);
                    self.stack.push(fst);
                }
                Rot => {
                    if self.stack.len() < 3 {
                        // Error
                        unimplemented!();
                    }

                    let c = self.stack.pop().unwrap();
                    let b = self.stack.pop().unwrap();
                    let a = self.stack.pop().unwrap();
                    self.stack.extend([b, c, a]);
                }
                Print => {
                    if self.stack.is_empty() {
                        // Error
                        unimplemented!("Unhandled error: Too few stack items for Opcode::Print");
                    }

                    let n = self.stack.pop().unwrap();
                    print!("{} ", n);
                }
                ShowStack => {
                    print!("<{}> ", self.stack.len());
                    self.stack.iter().enumerate().for_each(|(i, elem)| {
                        if i > 0 {
                            print!(" ");
                        }
                        print!("{}", elem);
                    });
                    print!(" ");
                }
                CompileMode => self.mode = Mode::Compiling,
                InterpretMode => self.mode = Mode::Interpreting,
            }
            ip += 1;
        }
    }

    pub fn interpret(&mut self, input: &str) {
        let mut tokens = Lexer::new(input);

        while let Some(token) = tokens.next(&self.dict) {
            let (op_code, imm) = match token {
                Token::Num(number) => (OpCode::Push(number), false),
                Token::Word(idx, imm) => (OpCode::Call(idx), imm),
                Token::Undefined(_) => unimplemented!(),
            };

            match (&self.mode, imm) {
                (Mode::Interpreting, _) | (_, true) => self.exec(&[op_code], &mut tokens),
                (Mode::Compiling, false) => {
                    if self.stack.is_empty() {
                        // Error
                        unimplemented!()
                    }
                    let idx: usize = (*self.stack.last().unwrap()).try_into().unwrap();
                    self.dict[idx].instrs.push(op_code)
                }
            };
        }

        println!("ok");
    }
}

fn main() -> Result<()> {
    let args = Args::parse();

    let mut machine = Machine::default();

    if let Some(expr) = args.expression {
        // Exec input and return.
        machine.interpret(expr.as_str());
        return Ok(());
    }

    let mut rl = Editor::<()>::new()?;
    if rl.load_history(HISTORY_FILE).is_err() {
        println!("Starting new history at {}", HISTORY_FILE);
    }

    loop {
        match rl.readline(PROMPT) {
            Ok(line) => {
                rl.add_history_entry(line.as_str());
                machine.interpret(line.as_str());
            }
            Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => {
                break;
            }
            Err(err) => {
                println!("Error: {:?}", err);
            }
        }
    }

    rl.save_history(HISTORY_FILE)?;
    Ok(())
}
